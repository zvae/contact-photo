package com.lszvae;

import android.app.Activity;
import android.app.Application;
import android.os.Handler;

import com.blankj.utilcode.util.ActivityUtils;
import com.blankj.utilcode.util.AppUtils;
import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.Utils;

import java.util.List;


public class ContactApp extends Application {

	private static ContactApp contactApp = null;
    private Handler handler = new Handler();

	public static synchronized ContactApp getInstance() {
		return contactApp;
	}
    /**
     * 获得当前app运行的AppContext
     *
     * @return
     */
    public static ContactApp getApp() {
        return (ContactApp) getInstance();
    }

	@Override
	public void onCreate() {
		super.onCreate();
		contactApp = this;
		init();
	}

	protected void init() {

        AppUtils.registerAppStatusChangedListener("appStatus", new Utils.OnAppStatusChangedListener() {
            @Override
            public void onForeground() {
                LogUtils.d(">>>>>>>>>>>>>>>>>>>App切到前台");
//                FloatWindow.destroy();
				List<Activity> activityList = ActivityUtils.getActivityList();
				for (Activity activity : activityList) {
					if (activity instanceof MainActivity){
						((MainActivity) activity).reloadData();
					}
				}
			}
 
            @Override
            public void onBackground() {
                LogUtils.d("《《《《《《《《《《<<<<< App切到后台");
//                initFloatWindow();
            }
        });
//        initFloatWindow();
 
    }
 

 
}