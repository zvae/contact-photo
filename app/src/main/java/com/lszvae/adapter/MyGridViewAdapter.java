package com.lszvae.adapter;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.lszvae.MainActivity;
import com.lszvae.R;
import com.lszvae.bean.MyContact;

import java.util.ArrayList;
import java.util.List;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

/**
 * 自定义Adapter
 * Created by Spring on 2015/11/28.
 */
public class MyGridViewAdapter extends BaseAdapter {
	private static final String TAG = "gradViewAdapter";
	private Context context;
	private MainActivity mainActivity;
	private List<MyContact> contractList = new ArrayList<>();

	public MyGridViewAdapter(Context context) {
		super();
		this.context = context;
	}

	@Override
	public int getCount() {
		return contractList.size();
	}

	@Override
	public Object getItem(int position) {
//		Log.e(TAG, "getItem 位置： " + position);
		return position;
	}


	@Override
	public long getItemId(int position) {
//		Log.e(TAG, "getItemId 位置： " + position);
		return position;
	}


	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
//		Log.e(TAG, "getView 位置： " + position);
		ViewHolder holder;
		if (convertView == null) {
			convertView = LayoutInflater.from(context).inflate(R.layout.main_item, parent, false);
			holder = new ViewHolder();
			holder.imageView = (ImageView) convertView.findViewById(R.id.item_img);
			holder.textView = (TextView) convertView.findViewById(R.id.item_txt);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}
		MyContact myContact = contractList.get(position);
		String photo = myContact.getPhotoUri();
		if (photo == null || "".equals(photo)) {
			holder.imageView.setImageResource(R.drawable.default_contact);
		} else {
			try {
				Uri photoUri = Uri.parse(photo);
				holder.imageView.setImageURI(photoUri);
			} catch (Exception e) {
				holder.imageView.setImageResource(R.drawable.default_contact);
			}
		}
		holder.textView.setText(myContact.getName());
		return convertView;
	}

	public List<MyContact> getContractList() {
		return contractList;
	}

	public void setContractList(List<MyContact> contractList) {
		this.contractList = contractList;
	}

	class ViewHolder {
		ImageView imageView;
		TextView textView;
	}

}