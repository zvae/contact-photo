package com.lszvae;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Toast;

import com.blankj.utilcode.util.LogUtils;
import com.blankj.utilcode.util.SPUtils;
import com.blankj.utilcode.util.Utils;
import com.lszvae.adapter.MyGridViewAdapter;
import com.lszvae.bean.MyContact;
import com.lszvae.view.MyGridView;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static com.blankj.utilcode.util.ActivityUtils.startActivity;

public class MainActivity extends AppCompatActivity {

	private final String TAG = "TAG";
	private MyGridView gridView;
	private MyGridViewAdapter adapter = null;
	private List<MyContact> contractList = new ArrayList<>();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_main);
		super.onCreate(savedInstanceState);
		this.initView();

	}

	private void initView() {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_main);
		gridView = (MyGridView) this.findViewById(R.id.main_gridview);
		adapter = new MyGridViewAdapter(this);
		gridView.setAdapter(adapter);
		reloadData();
		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				String phone = contractList.get(position).getPhone();
				LogUtils.d("给[ " + position + " ] 打电话：", phone);
				callPhone(phone);
			}
		});
		gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {

				return true;
			}
		});
	}

	/**
	 * 拨打电话（直接拨打电话）
	 * @param phoneNum 电话号码
	 */
	private void callPhone(String phoneNum){
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
				!= PackageManager.PERMISSION_GRANTED) {
			Toast.makeText(this, "获取拨打电话权限失败，请先打开权限", Toast.LENGTH_SHORT).show();
			return;
		}
		Intent intent = new Intent(Intent.ACTION_CALL);
		Uri data = Uri.parse("tel:" + phoneNum);
		intent.setData(data);
		startActivity(intent);
	}

	/**
	 * 重新加载数据
	 */
	public void reloadData(){
		//判断用户是否已经授权给我们了 如果没有，调用下面方法向用户申请授权，之后系统就会弹出一个权限申请的对话框
		// 获取读取联系人的权限
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS)
				!= PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(
					MainActivity.this, new String[]{Manifest.permission.READ_CONTACTS}, 1);
		} else {
			readContacts();
		}
	}

	/**
	 * 	调用并获取联系人信息
	 */
	private void readContacts() {
		// 拨打电话权限
		if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE)
				!= PackageManager.PERMISSION_GRANTED) {
			ActivityCompat.requestPermissions(
					MainActivity.this, new String[]{Manifest.permission.CALL_PHONE}, 2);
		}
		Cursor cursor = null;
		try {
			Set<MyContact> list = new HashSet<>();
			cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
					null, null, null, null);
			if (cursor == null) {
				return;
			}
			while (cursor.moveToNext()) {
				//得到联系人名称
				String displayName = cursor.getString(cursor.getColumnIndex(
						ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));
				String number = cursor.getString(cursor.getColumnIndex(
						ContactsContract.CommonDataKinds.Phone.NUMBER));

				//得到联系人ID
				Long contactid = cursor.getLong(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.CONTACT_ID));
				//得到联系人头像ID
				Long photoid = cursor.getLong(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_ID));
				String photoUri = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI));


//				LogUtils.e(cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_ID)),
//						cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_FILE_ID)),
//						cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_THUMBNAIL_URI)),
//						cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.PHOTO_URI)));

				MyContact myContact = new MyContact(1L, displayName, contactid, number, photoid, photoUri);
				list.add(myContact);
			}
			this.contractList = new ArrayList<>(list);
			adapter.setContractList(contractList);
			//刷新
			adapter.notifyDataSetChanged();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (cursor != null) {
				cursor.close();
			}
		}


	}

	//回调方法，无论哪种结果，最终都会回调该方法，之后在判断用户是否授权，
	// 用户同意则调用readContacts（）方法，失败则会弹窗提示失败
	@Override
	public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
		switch (requestCode) {
			case 1:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
					readContacts();
				} else {
					Toast.makeText(this, "获取联系人权限失败", Toast.LENGTH_SHORT).show();
				}
				break;
			case 2:
				if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
				} else {
					Toast.makeText(this, "获取拨打电话权限失败", Toast.LENGTH_SHORT).show();
				}
				break;
			default:
		}
	}


}
