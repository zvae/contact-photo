package com.lszvae.bean;

import java.util.Objects;

/**
 * @author zvae
 * @date 2020/9/30 10:27
 */
public class MyContact {
	private Long index;
	private String name;
	private Long contactId;
	private String phone;

	private Long photoId;
	private String photoUri;


	public MyContact() {
	}

	public MyContact(Long index, String name, Long contactId, String phone, Long photoId, String photoUri) {
		this.index = index;
		this.name = name;
		this.contactId = contactId;
		this.phone = phone;
		this.photoId = photoId;
		this.photoUri = photoUri;
	}



	public Long getIndex() {
		return index;
	}

	public void setIndex(Long index) {
		this.index = index;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getContactId() {
		return contactId;
	}

	public void setContactId(Long contactId) {
		this.contactId = contactId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Long getPhotoId() {
		return photoId;
	}

	public void setPhotoId(Long photoId) {
		this.photoId = photoId;
	}

	public String getPhotoUri() {
		return photoUri;
	}

	public void setPhotoUri(String photoUri) {
		this.photoUri = photoUri;
	}

	@Override
	public String toString() {
		return "MyContact{" +
				"index=" + index +
				", contactId=" + contactId +
				", phone='" + phone + '\'' +
				", photoId=" + photoId +
				", photoUri='" + photoUri + '\'' +
				'}';
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		MyContact myContact = (MyContact) o;
		return Objects.equals(index, myContact.index) &&
				Objects.equals(name, myContact.name) &&
				Objects.equals(contactId, myContact.contactId) &&
				Objects.equals(phone, myContact.phone) &&
				Objects.equals(photoId, myContact.photoId) &&
				Objects.equals(photoUri, myContact.photoUri);
	}

	@Override
	public int hashCode() {
		return Objects.hash(index, name, contactId, phone, photoId, photoUri);
	}
}
